﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Qmuli.Adlib.Client.Dto;
using Qmuli.Adlib.Client.Dto.PublicDataV5;
using System.Web;
using Newtonsoft.Json;

namespace Qmuli.Adlib.Client
{
    /// <summary>
    /// Adlib Public Data Api Client (For Version 5)
    /// </summary>
    public static class PublicDataApiV5
    {
        /// <summary>
        /// Gets the session Key for the specified credentials
        /// </summary>
        /// <returns></returns>
        public static string GetSessionKey(string password, RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var content = new
                {
                    ApiKey = (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataApiKey,
                    Username = (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataApiUsername,
                    Password = password
                };
                //client.DefaultRequestHeaders.Add("Content-Type", "application/json");

                //client.DefaultRequestHeaders.TryAddWithoutValidation("ApiKey", (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataApiKey);
                //client.DefaultRequestHeaders.TryAddWithoutValidation("Username", (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataApiUsername);
                //client.DefaultRequestHeaders.TryAddWithoutValidation("Password", password);
                //var content = new FormUrlEncodedContent(new[]
                //{
                //    new KeyValuePair<string, string>("ApiKey", (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataApiKey),
                //    new KeyValuePair<string, string>("Username", (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataApiUsername),
                //    new KeyValuePair<string, string>("Password", password)
                //});

                var json = JsonConvert.SerializeObject(content);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = client.PostAsync("GetSessionKey/", stringContent).Result;

                var result = String.Empty;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result ?? String.Empty;
                }

                return result;
            }
        }

        /// <summary>
        /// Checks is the supplied headers are valid
        /// </summary>
        /// <returns></returns>
        public static bool IsSessionKeyValid(RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("ApiKey", (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataApiKey);
                client.DefaultRequestHeaders.TryAddWithoutValidation("Username", (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataApiUsername);
                client.DefaultRequestHeaders.TryAddWithoutValidation("SessionKey", (requestSettings ?? DefaultRequestSettings).QmuliAdlibPublicDataSessionKey);

                var response = client.PostAsync("IsSessionKeyValid/", null).Result;

                var result = false;

                if (response.IsSuccessStatusCode)
                {
                    result = (response.Content.ReadAsStringAsync().Result.ToLower() == "true");
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the date for the most recent publication update
        /// </summary>
        /// <returns></returns>
        public static DateTimeOffset GetPublicationMostRecentUpdatedDate(RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {   
                var response = client.GetAsync("GetPublicationMostRecentUpdatedDate/").Result;

                var result = DateTimeOffset.MinValue;

                if (response.IsSuccessStatusCode)
                {
                    result = DateTimeOffset.Parse(response.Content.ReadAsAsync<string>().Result);
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the date for the most recent publication update (as a string returned by the server)
        /// </summary>
        /// <returns></returns>
        public static string GetPublicationMostRecentUpdatedDateAsString(RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var response = client.GetAsync("GetPublicationMostRecentUpdatedDate/").Result;

                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<string>().Result;
                }

                return "";
            }
        }

        /// <summary>
        ///  	Gets the Publications Details, including Id, name, contact details and image urls for the related publication Id
        /// </summary>
        /// <param name="publicationId">PublicationId</param>
        /// <returns></returns>
        public static PublisherData GetPublisherDataFromPublicationId(int publicationId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublisherFromPublicationId/?PublicationId={0}", publicationId);
            var result = MakeRequest<PublisherData>(address, requestSettings);
            return result ?? new PublisherData();
        }

        /// <summary>
        /// Gets the Publisher Details, including Id, name, contact details and image urls for the specified publisher Id
        /// </summary>
        /// <param name="publisherId">PublisherId</param>
        /// <returns></returns>
        public static PublisherData GetPublisher(int publisherId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublisher/?PublisherId={0}", publisherId);
            var result = MakeRequest<PublisherData>(address, requestSettings);
            return result ?? new PublisherData();
        }

        /// <summary>
        /// Gets the Media Owner's Details.
        /// </summary>
        /// <param name="mediaOwnerId">Media Owner's</param>
        /// <returns></returns>
        public static PublisherData GetMediaOwner(int mediaOwnerId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetMediaOwner/?mediaOwnerId={0}", mediaOwnerId);
            var result = MakeRequest<PublisherData>(address, requestSettings);
            return result ?? new PublisherData();
        }

        /// <summary>
        /// Gets the Publisher Details, including Id, name, contact details and image urls for the specified publisher Id
        /// </summary>
        /// <param name="publisherId">PublisherId</param>
        /// <returns></returns>
        public static PublisherData GetPressSectionsPublication(int sectionId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPressSectionsPublication/?sectionId={0}", sectionId);
            var result = MakeRequest<PublisherData>(address, requestSettings);
            return result ?? new PublisherData();
        }

        /// <summary>
        /// Gets the Publisher Details List from the Adlib Public Data Api
        /// </summary>
        /// <param name="query"></param>
        /// <param name="onlyReturnPublicationsDeliverableViaAdfast"></param>
        /// <param name="publishedOnly"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public static List<PublisherData> GetPublisherList(string query, bool? onlyReturnPublicationsDeliverableViaAdfast, bool? publishedOnly, int? countryId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublisherList/?Query={0}&OnlyReturnPublicationsDeliverableViaAdfast={1}&PublishedOnly={2}&CountryId={3}", query, onlyReturnPublicationsDeliverableViaAdfast, publishedOnly, countryId);
            var result = MakeRequest<IEnumerable<PublisherData>>(address, requestSettings).ToList();
            return result ?? new List<PublisherData>();
        }

        /// <summary>
        /// Gets the Press Centre Select List from the Adlib Public Data Api.
        /// The Press Centre is the grouping of Publications that get produced by a specific centre/location. This list may result in many items for Publisher.
        /// This list is typically used in Qmuli Adfast, see GetPublisherSelectList for a list of distinct Publishers.
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="publisherId">Publisher Id</param>
        /// <param name="onlyReturnPublicationsDeliverableByAdfast"></param>
        /// <returns>SelectListItem List</returns>
        public static List<SelectListOption> GetPressCentreSelectList(string query, int? publisherId, bool? onlyReturnPublicationsDeliverableByAdfast, RequestSettings requestSettings = null)
        {
            var address = $"GetPressCentreSelectList/?Query={query}&PublisherId={publisherId}&OnlyReturnPublicationsDeliverableByAdfast={onlyReturnPublicationsDeliverableByAdfast}";
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        /// <summary>
        /// Gets the Press Centre List from the Adlib Public Data Api that are published and contain the query.
        /// </summary>
        /// <param name="query">Search query</param>
        /// <returns>A collection of PressCentreDto</returns>
        public static IEnumerable<PressCentreDto> GetWebsitePressCentreSearch(string query, RequestSettings requestSettings = null)
        {
            var encodedQuery = HttpUtility.UrlEncode(query);
            var address = $"GetPressCentreSearch/?Query={encodedQuery}&PublishedOnly=true";
            var result = MakeRequest<IEnumerable<PressCentreDto>>(address, requestSettings);
            return result ?? new List<PressCentreDto>();
        }

        /// <summary>
        /// Gets the Press Centre Select List from the Adlib Public Data Api.
        /// The Press Centre is the grouping of Publications that get produced by a specific centre/location. This list may result in many items for Publisher.
        /// This list is typically used in Qmuli Adfast, see GetPublisherSelectList for a list of distinct Publishers.
        /// </summary>
        /// <param name="countryId">Country to filter with</param>
        /// <returns>Press centress converted to a SelectListItem List</returns>
        public static List<SelectListOption> GetAdfastPressCentreSelectList(int? countryId, RequestSettings requestSettings = null)
        {
            var address = $"GetPressCentreSelectList/?CountryId={countryId}&OnlyReturnPublicationsDeliverableByAdfast=true";
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        /// <summary>
        /// Gets the Publication Details List list from the Adlib Public Data Api
        /// </summary>
        /// <param name="publisherId"></param>
        /// <param name="query"></param>
        /// <param name="fromLastUpdatedDate"></param>
        /// <returns></returns>
        public static List<PublicationData> GetPublicationList(int? publisherId, string query, DateTime? fromLastUpdatedDate, bool includeDeleted = true, bool onlyPublishedItems = false, int countryId = -1, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublicationList/?PublisherId={0}&Query={1}&FromLastUpdatedDate={2}&includeDeleted={3}&PublishedOnly={4}&countryId={5}", publisherId,
                    query, (fromLastUpdatedDate.HasValue ? fromLastUpdatedDate.Value.ToString("G") : String.Empty), includeDeleted, onlyPublishedItems, countryId);
            var result = MakeRequest<IEnumerable<PublicationData>>(address, requestSettings).ToList();
            return result ?? new List<PublicationData>();
        }

        /// <summary>
        /// Gets a list of publications belonging to the media owner.
        /// </summary>
        /// <param name="pressCentreId"></param>
        /// <returns></returns>
        public static IEnumerable<PublicationData> GetPressCentresPublicationList(int? pressCentreId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPressCentresPublicationList/?pressCentreId={0}", pressCentreId);
            var result = MakeRequest<IEnumerable<PublicationData>>(address, requestSettings);
            return result ?? new List<PublicationData>();
        }

        /// <summary>
        /// Gets a list of publications belonging to the media owner.
        /// </summary>
        /// <param name="mediaOwnerId"></param>
        /// <returns></returns>
        public static IEnumerable<PublicationData> GetMediaOwnersPublicationList(int? mediaOwnerId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetMediaOwnersPublicationList/?mediaOwnerId={0}", mediaOwnerId);
            var result = MakeRequest<IEnumerable<PublicationData>>(address, requestSettings);
            return result ?? new List<PublicationData>();
        }

        /// <summary>
        ///  	Gets the Publications Details, including Id, name, contact details and image urls for the specified publication Id
        /// </summary>
        /// <param name="publicationId">PublicationId</param>
        /// <param name="includeDeleted"></param>
        /// <param name="onlyPublishedItems"></param>
        /// <returns></returns>
        public static PublicationData GetPublication(int publicationId, bool includeDeleted = true, bool onlyPublishedItems = false, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublication/?PublicationId={0}&includeDeleted={1}", publicationId, includeDeleted);
            var result = MakeRequest<PublicationData>(address, requestSettings);
            return result ?? new PublicationData();
        }

        /// <summary>
        /// Returns a list of updated Publications Details from the specified date
        /// (updated date could include date of publication or related data, eg section and size data)
        /// </summary>
        /// <param name="fromDate">The minimum date to check for updated data in the format: dd/MM/yyyy hh:mm:ss</param>
        /// <param name="includeDeleted"></param>
        /// <param name="onlyPublishedItems"></param>
        /// <returns></returns>
        public static List<PublicationChangedData> GetUpdatedPublicationList(string fromDate, bool includeDeleted = true, bool onlyPublishedItems = false, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetUpdatedPublicationList/?FromDate={0}&includeDeleted={1}", fromDate, includeDeleted);
            var result = MakeRequest<IEnumerable<PublicationChangedData>>(address, requestSettings).ToList();
            return result ?? new List<PublicationChangedData>();
        }

        /// <summary>
        /// Gets the Publication Id List list from the last updated date
        /// </summary>
        /// <param name="query"></param>
        /// <param name="fromLastUpdatedDate"></param>
        /// <param name="returnDeliverableByAdfastOnly"></param>
        /// <returns></returns>
        public static List<int> GetPublicationIdListFromLastUpdatedDate(string query, DateTime? fromLastUpdatedDate, bool? returnDeliverableByAdfastOnly, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublicationIdListFromLastUpdatedDate/?FromDate={0}&ReturnDeliverableByAdfastOnly={1}", fromLastUpdatedDate.HasValue ? fromLastUpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : String.Empty, returnDeliverableByAdfastOnly);
            var result = MakeRequest<IEnumerable<int>>(address, requestSettings).ToList();
            return result ?? new List<int>();
        }

        /// <summary>
        /// Gets the Publication Select List list from the Adlib Public Data Api
        /// </summary>
        /// <param name="publisherId"></param>
        /// <param name="query"></param>
        /// <param name="onlyReturnPublicationsDeliverableViaAdfast"></param>
        /// <returns></returns>
        public static List<SelectListOption> GetPublicationSelectList(int? publisherId, string query, bool? onlyReturnPublicationsDeliverableViaAdfast, RequestSettings requestSettings = null)
        {
            var encodedQuery = HttpUtility.UrlEncode(query);
            var address = String.Format("GetPublicationSelectList/?PublisherId={0}&Query={1}&OnlyReturnPublicationsDeliverableViaAdfast=", publisherId, encodedQuery, onlyReturnPublicationsDeliverableViaAdfast);
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        /// <summary>
        /// Gets a Publication List from the Adlib Public Data Api that are published and contain the query.
        /// </summary>
        /// <param name="publisherId">Id of the publisher whose publications are searched (optional)</param>
        /// <param name="query">search phrase</param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static IEnumerable<PublicationSearchDto> GetWebsitePublicationSearch(int? publisherId, string query, RequestSettings requestSettings = null)
        {
            var encodedQuery = HttpUtility.UrlEncode(query);
            var address = $"GetPublicationSearch/?Query={query}&PublisherId={publisherId}&PublishedOnly=true";
            var result = MakeRequest<IEnumerable<PublicationSearchDto>>(address, requestSettings);
            return result ?? new List<PublicationSearchDto>();
        }

        /// <summary>
        /// Gets the Publication Select List list from the Adlib Public Data Api by Press Centre Id.
        /// This is used by Adfast to filter by press centre (or multiple press centres)
        /// Have a look at GetPublicationSelectList for filtering by Publisher.
        /// </summary>
        /// <param name="pressCentreId"></param>
        /// <param name="query"></param>
        /// <param name="onlyReturnPublicationsDeliverableViaAdfast"></param>
        /// <param name="publishedInCountryId"></param>
        /// <param name="requestSettings"></param>
        /// <param name="excludePressCentre"></param>
        /// <param name="pressCentreList"></param>
        /// <returns></returns>
        public static List<SelectListOption> GetPublicationSelectListByPressCentre(int? pressCentreId, string query, bool? onlyReturnPublicationsDeliverableViaAdfast, int? publishedInCountryId, RequestSettings requestSettings = null, bool excludePressCentre = false, List<int> pressCentreList = null)
        {
            var encodedQuery = HttpUtility.UrlEncode(query);
            var encodedPressCentreList = HttpUtility.UrlEncode(string.Join(",", (pressCentreList ?? new List<int>()).Select(n => n.ToString()).ToArray()));
            var address = $"GetPublicationSelectListByPressCentre/?PressCentreId={pressCentreId}&Query={encodedQuery}&OnlyReturnPublicationsDeliverableViaAdfast={onlyReturnPublicationsDeliverableViaAdfast}&PublishedInCountryId={publishedInCountryId}&AppendCountry=true&ExcludePressCentre={excludePressCentre.ToString().ToLower()}&PressCentreList={encodedPressCentreList}";
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        /// <summary>
        /// Gets the Publication Select List list from the Adlib Public Data Api by Press Centre Id that are allowed for Admin+.
        /// Ha ve a look at GetPublicationSelectList for filtering by Publisher.
        /// </summary>
        /// <param name="pressCentreId"></param>
        /// <param name="query"></param>
        /// <param name="publishedInCountryId"></param>
        /// <param name="requestSettings"></param>
        /// <param name="excludePressCentre"></param>
        /// <returns></returns>
        public static List<SelectListOption> GetAdminPlusPublicationSelectListByPressCentre(int? pressCentreId, string query, int? publishedInCountryId, RequestSettings requestSettings = null, bool excludePressCentre = false)
        {
            var encodedQuery = HttpUtility.UrlEncode(query);
            var address = $"GetPublicationSelectListByPressCentre/?PressCentreId={pressCentreId}&Query={encodedQuery}&AdminPlusOnly=true&PublishedInCountryId={publishedInCountryId}&ExcludePressCentre={excludePressCentre.ToString().ToLower()}";
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        /// <summary>
        /// Gets the Press Sections for the specified publication Id for the purposes of using for a Select List
        /// </summary>
        /// <param name="publicationId"></param>
        /// <returns></returns>
        public static List<SelectListOption> GetPublicationPressSectionSelectList(int publicationId, RequestSettings requestSettings = null)
        {
            var address = $"GetPublicationPressSectionSelectList/?PublicationId={publicationId}";
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        /// <summary>
        /// Gets the Press Sections for the specified publication Id for the purposes of using for a Select List
        /// </summary>
        /// <param name="publicationId"></param>
        /// <returns></returns>
        public static List<SelectListOption> GetAdfastPublicationPressSectionSelectList(int publicationId, RequestSettings requestSettings = null)
        {
            var address = $"GetPublicationPressSectionSelectList/?PublicationId={publicationId}&AdfastOnly=true&PublishedOnly=false";
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        /// <summary>
        /// Gets the Press Sections for the specified publication Id for the purposes of using for a Select List
        /// </summary>
        /// <param name="publicationId"></param>
        /// <returns></returns>
        public static List<SelectListOption> GetAdminPlusPublicationPressSectionSelectList(int publicationId, RequestSettings requestSettings = null)
        {
            var address = $"GetPublicationPressSectionSelectList/?PublicationId={publicationId}&AdminPlusOnly=true&PublishedOnly=false";
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        public static List<PublicationSectionSize> GetPublicationPressSectionSizes(int pressSectionId, string units, RequestSettings requestSettings = null)
        {
            var address = $"GetPublicationPressSectionSizes/?PressSectionId={pressSectionId}&Units={units}";
            var result = MakeRequest<IEnumerable<PublicationSectionSize>>(address, requestSettings).ToList();
            return result ?? new List<PublicationSectionSize>();
        }

        /// <summary>
        /// Gets the specified press section
        /// </summary>
        /// <param name="pressSectionId">Id of the press section</param>
        /// <param name="units">milimeters or inches (if null then milimeters are assumed)</param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static PublicationPressSection GetPressSection(int pressSectionId, string units, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPressSection/?PressSectionId={0}&Units={1}", pressSectionId, units);
            var result = MakeRequest<PublicationPressSection>(address, requestSettings);
            return result ?? new PublicationPressSection();
        }

        /// <summary>
        /// Gets the publication press sections and related sizes
        /// </summary>
        /// <param name="publicationId">Publication Id</param>
        /// <param name="units">Units of measure (Inches or Millimeters)</param>
        /// <param name="onlyPublished"></param>
        /// <param name="includeDeleted"></param>
        /// <returns></returns>
        public static List<PublicationPressSection> GetPublicationPressSections(int publicationId, string units, bool onlyPublished = false, bool includeDeleted = false, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublicationPressSections/?PublicationId={0}&Units={1}&PublishedOnly={2}&includeDeleted={3}", publicationId, units, onlyPublished, includeDeleted);
            var result = MakeRequest<IEnumerable<PublicationPressSection>>(address, requestSettings).ToList();
            return result ?? new List<PublicationPressSection>();
        }

        /// <summary>
        /// Gets the publication press sections and related sizes
        /// </summary>
        /// <param name="publicationId">Publication Id</param>
        /// <param name="units">Units of measure (Inches or Millimeters)</param>
        /// <param name="onlyPublished"></param>
        /// <param name="includeDeleted"></param>
        /// <returns></returns>
        public static List<PublicationPressSection> GetAdminPlusPublicationPressSections(int publicationId, string units, bool includeDeleted = false, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublicationPressSections/?PublicationId={0}&Units={1}&AdminPlusOnly=true&includeDeleted={2}", publicationId, units, includeDeleted);
            var result = MakeRequest<IEnumerable<PublicationPressSection>>(address, requestSettings).ToList();
            return result ?? new List<PublicationPressSection>();
        }

        /// <summary>
        /// Gets the publication digital sections and related sizes
        /// </summary>
        /// <param name="publicationId">Publication Id</param>
        /// <param name="onlyPublished"></param>
        /// <param name="includeDeleted"></param>
        /// <returns></returns>
        public static List<PublicationDigitalSection> GetPublicationDigitalSections(int publicationId, bool onlyPublished = false, bool includeDeleted = false, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublicationDigitalSections/?PublicationId={0}&PublishedOnly={1}&includeDeleted{2}", publicationId, onlyPublished, includeDeleted);
            var result = MakeRequest<IEnumerable<PublicationDigitalSection>>(address, requestSettings);
            return result != null ? result.ToList() : new List<PublicationDigitalSection>();
        }

        /// <summary>
        /// Gets the publication digital sections sizes
        /// </summary>
        /// <param name="publicationSectionId">Publication Section Id (digital section)</param>
        /// <returns></returns>
        public static List<DigitalSize> GetPublicationDigitalSectionSizes(int publicationSectionId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublicationDigitalSectionSizes/?DigitalSectionId={0}", publicationSectionId);
            var result = MakeRequest<IEnumerable<DigitalSize>>(address, requestSettings).ToList();
            return result ?? new List<DigitalSize>();
        }

        /// <summary>
        /// Returns a list of digital publication sections for the purpose of populating a select list
        /// </summary>
        /// <param name="publicationId">Search query</param>
        /// <param name="onlyPublished"></param>
        /// <param name="includeDeleted"></param>
        /// <returns>SelectListItem List</returns>
        public static List<SelectListOption> GetPublicationDigitalSectionSelectList(int publicationId, bool onlyPublished = false, bool includeDeleted = false, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetPublicationDigitalSectionSelectList/?PublicationId={0}&PublishedOnly={1}&includeDeleted={2}", publicationId, onlyPublished, includeDeleted);
            var result = MakeRequest<IEnumerable<SelectListOption>>(address, requestSettings).ToList();
            return result ?? new List<SelectListOption>();
        }

        /// <summary>
        /// Gets the Digital Size for the specified digitalSizeId
        /// </summary>
        /// <param name="digitalSizeId">Search query</param>
        public static DigitalSize GetPublicationSectionDigitalSize(int digitalSizeId, RequestSettings requestSettings = null)
        {
            var address = String.Concat("GetPublicationSectionDigitalSize/?DigitalSectionSizeId=", digitalSizeId);
            var result = MakeRequest<DigitalSize>(address, requestSettings);
            return result ?? new DigitalSize();
        }

        /// <summary>
        /// Gets the Digital Section pricing data for delivery and production (This is used by Qmuli internally, this call is not available to standard users)
        /// </summary>
        /// <param name="digitalSectionId">Digital Section Id</param>
        /// <returns></returns>
        public static PriceListData GetDigitalSectionPricing(int digitalSectionId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetDigitalSectionPricing/?DigitalSectionId={0}", digitalSectionId);
            var result = MakeRequest<PriceListData>(address, requestSettings);
            return result ?? new PriceListData();
        }

        /// <summary>
        /// Gets the contacts of a press or digital section.
        /// </summary>
        /// <returns>A collection of contacts</returns>
        public static IEnumerable<PublicationContactDto> GetSectionContacts(int sectionId, RequestSettings requestSettings = null)
        {
            var address = String.Format("GetSectionContacts/?sectionId={0}", sectionId);
            var result = MakeRequest<IEnumerable<PublicationContactDto>>(address, requestSettings);
            return result ?? new List<PublicationContactDto>();
        }

        /// <summary>
        /// Gets the full api version number in format x.x.x
        /// </summary>
        /// <returns></returns>
        public static string GetApiVersion(RequestSettings requestSettings = null)
        {
            var result = MakeRequest<string>("GetApiVersion/", requestSettings);
            return result ?? string.Empty;
        }

        public static List<Country> GetCountryList(RequestSettings requestSettings = null)
        {
            var countryList = MakeRequest<IEnumerable<Country>>("GetCountryList", requestSettings).ToList();
            return countryList;
        }

        /// <summary>
        /// Gets Qmuli Standards's section IDs and their Titles (publication names) for sections called "All Sections".
        /// </summary>
        /// <returns>Section Id and Title (publication name)</returns>
        public static IEnumerable<TitleSection> GetQmuliStandardWorkflows(RequestSettings requestSettings = null)
        {
            var result = MakeRequest<IEnumerable<TitleSection>>("GetQmuliStandardWorkflows", requestSettings).ToList();
            return result;
        }

        /// <summary>
        /// Gets the Adlib Press Centre (publisher), Publication, Section and Size Id and Name info for the specified PaperMule mapping codes andf sizes.
        /// </summary>
        /// <returns>PaperMuleMappingInfo Object</returns>
        public static PaperMuleMappingInfo GetPaperMuleMappings(string publisherCode, string publicationCode, string sectionCode, decimal height, decimal width, RequestSettings requestSettings = null)
        {
            var address = $"GetPaperMuleMappings/?publisherCode={HttpUtility.UrlEncode(publisherCode)}&publicationCode={HttpUtility.UrlEncode(publicationCode)}&sectionCode={HttpUtility.UrlEncode(sectionCode)}&height={HttpUtility.UrlEncode(height.ToString(CultureInfo.InvariantCulture))}&width={HttpUtility.UrlEncode(width.ToString(CultureInfo.InvariantCulture))}";
            var result = MakeRequest<PaperMuleMappingInfo>(address, requestSettings);
            return result ?? new PaperMuleMappingInfo();
        }

        /// <summary>
        /// Gets the Adlib Press Centre (publisher), Publication, Section and Size Id and Name info for the specified PaperMule mapping codes andf sizes.
        /// </summary>
        /// <param name="publisherCode"></param>
        /// <param name="publicationCode"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="requestSettings"></param>
        /// <returns>PublicationSectionMappingInfo</returns>
        public static PublicationSectionMappingInfo GetPublicationSectionMappings(string publisherCode, string publicationCode, decimal height, decimal width, RequestSettings requestSettings = null)
        {
            var address = $"GetPublicationSectionMappings/?publisherCode={HttpUtility.UrlEncode(publisherCode)}&publicationCode={HttpUtility.UrlEncode(publicationCode)}&height={HttpUtility.UrlEncode(height.ToString(CultureInfo.InvariantCulture))}&width={HttpUtility.UrlEncode(width.ToString(CultureInfo.InvariantCulture))}";
            var result = MakeRequest<PublicationSectionMappingInfo>(address, requestSettings);
            return result ?? new PublicationSectionMappingInfo();
        }

        /// <summary>
        /// Gets the list of GePublication Billing Units (restricted access for internal use)
        /// </summary>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static List<BillingUnit> GetPublicationBillingUnits(RequestSettings requestSettings = null)
        {
            var billingUnits = MakeRequest<IEnumerable<BillingUnit>>("GetPublicationBillingUnits", requestSettings).ToList();
            return billingUnits;
        }

        private static T MakeRequest<T>(string address, RequestSettings requestSettings) where T :class
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var response = client.GetAsync(address).Result;

                T result = null;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<T>().Result;
                }

                return result;
            }
        }
        
        /// <summary>
        /// Gets the HttpClient and sets the required headers, with config values
        /// </summary>
        /// <returns></returns>
        private static HttpClient GetHttpClient(RequestSettings requestSettings)
        {
            if (requestSettings == null)
            {
                requestSettings = new RequestSettings
                {
                    QmuliAdlibPublicDataApiKey = Settings.QmuliAdlibPublicDataApiKey,
                    QmuliAdlibPublicDataApiUrl = Settings.QmuliAdlibPublicDataApiUrl,
                    QmuliAdlibPublicDataApiUsername = Settings.QmuliAdlibPublicDataApiUsername,
                    QmuliAdlibPublicDataSessionKey = Settings.QmuliAdlibPublicDataSessionKey
                };
            }

            var client = new HttpClient {BaseAddress = new Uri(requestSettings.QmuliAdlibPublicDataApiUrl)};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("SessionKey", requestSettings.QmuliAdlibPublicDataSessionKey);
            client.DefaultRequestHeaders.Add("Username", requestSettings.QmuliAdlibPublicDataApiUsername);
            return client;
        }

        public static RequestSettings DefaultRequestSettings
        {
            get
            {
                var result = new RequestSettings
                {
                    QmuliAdlibPublicDataApiUrl = Settings.QmuliAdlibPublicDataApiUrl,
                    QmuliAdlibPublicDataApiUsername = Settings.QmuliAdlibPublicDataApiUsername,
                    QmuliAdlibPublicDataApiKey = Settings.QmuliAdlibPublicDataApiKey
                };

                return result;
            }
        }
    }
}
