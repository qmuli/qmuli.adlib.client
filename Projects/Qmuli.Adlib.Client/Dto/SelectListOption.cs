﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto
{
    [Serializable, DataContract]
    public class SelectListOption
    {
        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string Text { get; set; }

        public SelectListOption()
        {
            Value = String.Empty;
            Text = String.Empty;
        }

        public SelectListOption(string value, string text)
        {
            Value = value;
            Text = text;
        }
    }
}