﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class DigitalSize
    {
        public DigitalSize()
        {
            Id = 0;
            Name = String.Empty;
            Width = 0;
            Height = 0;
            ExpandableWidth = 0;
            ExpandableHeight = 0;
            FlashSupported = false;
            VideoSupported = false;
            MaximumFileSizeText = String.Empty;
            MaximumFileSize = 0;
            DeliveryMethods = new List<PressSectionDeliveryMethodsData>();
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public float Width { get; set; }

        [DataMember]
        public float Height { get; set; }

        [DataMember]
        public float ExpandableWidth { get; set; }

        [DataMember]
        public float ExpandableHeight { get; set; }

        [DataMember]
        public bool FlashSupported { get; set; }

        [DataMember]
        public bool VideoSupported { get; set; }

        [DataMember]
        public string ExtraSizeInformation { get; set; }

        [DataMember]
        public string ExpandableExtraSizeInformation { get; set; }

        [DataMember]
        public bool DigitalWeightPublished { get; set; }

        [DataMember]
        public string MaximumFileSizeText { get; set; }

        [DataMember]
        public bool CommentsPublished { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public string Format { get; set; }

        [DataMember]
        public string Resolution { get; set; }

        [DataMember]
        public bool ColourSpacePublished { get; set; }

        [DataMember]
        public string ColourSpace { get; set; }

        [DataMember]
        public string ColourSpaceComments { get; set; }

        [DataMember]
        public int MaximumFileSize { get; set; }

        [DataMember]
        public string PoliteFileSize { get; set; }

        [DataMember]
        public string OBAFileSize { get; set; }

        [DataMember]
        public string UserInitiatedFileSize { get; set; }

        [DataMember]
        public string StreamFileSize { get; set; }

        [DataMember]
        public string WeightsComments { get; set; }

        [DataMember]
        public bool SafeAreaPublished { get; set; }

        [DataMember]
        public string SafeAreasComments { get; set; }

        [DataMember]
        public bool UrlLinkPublished { get; set; }

        [DataMember]
        public string UrlLinkComments { get; set; }

        [DataMember]
        public bool FlashPublished { get; set; }

        [DataMember]
        public string FlashComments { get; set; }

        [DataMember]
        public bool HTMLPublished { get; set; }

        [DataMember]
        public string HtmlComments { get; set; }

        [DataMember]
        public bool VideoPublished { get; set; }

        [DataMember]
        public string VideoFormat { get; set; }

        [DataMember]
        public string AnimationFrameRate { get; set; }

        [DataMember]
        public string VideoAspectRatio { get; set; }

        [DataMember]
        public string VideoAnimationLength { get; set; }

        [DataMember]
        public string VideoLength { get; set; }

        [DataMember]
        public string VideoComments { get; set; }

        [DataMember]
        public string VideoBitRate { get; set; }

        [DataMember]
        public string AudioInitiation { get; set; }

        [DataMember]
        public bool AudioPublished { get; set; }

        [DataMember]
        public string AudioFormat { get; set; }

        [DataMember]
        public string AudioComments { get; set; }

        [DataMember]
        public bool ClickTagPublished { get; set; }

        [DataMember]
        public string ClickTageComments { get; set; }

        [DataMember]
        public bool ThirdPartyServerPublished { get; set; }

        [DataMember]
        public string ThirdPartyServerComments { get; set; }

        [DataMember]
        public string DeliveryMethod { get; set; }

        [DataMember]
        public string CopyDeadline { get; set; }

        [DataMember]
        public string AditionalSectionInfo { get; set; }

        [DataMember]
        public string CustomSectionInfo { get; set; }

        [DataMember]
        public string PreviewUrl { get; set; }

        [DataMember]
        public string CustomPreviewUrl { get; set; }

        [DataMember]
        public bool Hotspot { get; set; }

        [DataMember]
        public bool HotSpotPublished { get; set; }

        [DataMember]
        public string HotSpotsComments { get; set; }

        [DataMember]
        public bool ShowExpandableSize { get; set; }

        [DataMember]
        public bool ControlsPublished { get; set; }

        [DataMember]
        public string ControlsComments { get; set; }

        [DataMember]
        public bool ButtonsPublished { get; set; }

        [DataMember]
        public string ButtonsComments { get; set; }

        [DataMember]
        public bool SlidePublished { get; set; }

        [DataMember]
        public string SlideComments { get; set; }

        [DataMember]
        public bool FlipPublished { get; set; }

        [DataMember]
        public string FlipComments { get; set; }

        [DataMember]
        public bool ZoomPublished { get; set; }

        [DataMember]
        public string ZoomComments { get; set; }

        [DataMember]
        public bool GesturesInteractionsPublished { get; set; }

        [DataMember]
        public string GesturesInteractionsComments { get; set; }

        [DataMember]
        public bool StructurePublished { get; set; }

        [DataMember]
        public string StructureComments { get; set; }

        [DataMember]
        public bool DatafilePublished { get; set; }

        [DataMember]
        public string DatafileComments { get; set; }

        [DataMember]
        public bool CPUUsagePublished { get; set; }

        [DataMember]
        public string CPUUsageComments { get; set; }

        [DataMember]
        public string CPUUsagePercentage { get; set; }

        [DataMember]
        public bool LabellingPublished { get; set; }

        [DataMember]
        public string LabellingComments { get; set; }

        [DataMember]
        public bool RangePublished { get; set; }

        [DataMember]
        public string ZIndexRange { get; set; }

        [DataMember]
        public string ZIndexComments { get; set; }

        [DataMember]
        public bool FontsPublished { get; set; }

        [DataMember]
        public string FontsComments { get; set; }

        [DataMember]
        public bool DeliveryPublished { get; set; }

        [DataMember]
        public IEnumerable<PressSectionDeliveryMethodsData> DeliveryMethods { get; set; }
    }
}
