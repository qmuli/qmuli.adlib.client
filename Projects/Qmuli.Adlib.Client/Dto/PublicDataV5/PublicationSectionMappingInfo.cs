﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class PublicationSectionMappingInfo
    {
        public PublicationSectionMappingInfo()
        {
            PressCentreId = 0;
            PressCentreName = string.Empty;
            PublicationId = 0;
            PublicationName = string.Empty;
            PublicationSectionId = 0;
            PublicationSectionName = string.Empty;
            PublicationSectionSizeId = 0;
            PublicationSectionSizeName = string.Empty;
        }

        /// <summary>
        /// List of Press Centres that match the supplied publisher code
        /// More than 1 if more tham 1 press centre has the same publisher code
        /// </summary>
        [DataMember]
        public List<int> PressCentreIdList { get; set; }

        /// <summary>
        /// Press centre Id if there 1 mapped press centre
        /// </summary>
        [DataMember]
        public int PressCentreId { get; set; }

        /// <summary>
        /// Press Centre name if there is 1 mapped press centre
        /// </summary>
        [DataMember]
        public string PressCentreName { get; set; }

        /// <summary>
        /// Publication Id
        /// </summary>
        [DataMember]
        public int PublicationId { get; set; }

        /// <summary>
        /// Publication Name
        /// </summary>
        [DataMember]
        public string PublicationName { get; set; }

        /// <summary>
        /// Publication Section Id
        /// </summary>
        [DataMember]
        public int PublicationSectionId { get; set; }

        /// <summary>
        /// Publication Section Name
        /// </summary>
        [DataMember]
        public string PublicationSectionName { get; set; }

        /// <summary>
        /// Publication Section Size Id
        /// </summary>
        [DataMember]
        public int PublicationSectionSizeId { get; set; }

        /// <summary>
        /// Publication Section Size Name
        /// </summary>
        [DataMember]
        public string PublicationSectionSizeName { get; set; }
    }
}
