﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class BillingUnit
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool SendInvoice { get; set; }

        [DataMember]
        public decimal? InvoicePercentage { get; set; }

        [DataMember]
        public string InvoiceEmailAddress { get; set; }

        [DataMember]
        public decimal? PublisherStandardDeliveryCost { get; set; }

        [DataMember]
        public decimal? PublisherMailBoxDeliveryCost { get; set; }

        [DataMember]
        public string InvoicePaymentMethod { get; set; }

        [DataMember]
        public string InvoiceAccountRef { get; set; }

        [DataMember]
        public string InvoiceRef { get; set; }

        [DataMember]
        public string BillingFrequency { get; set; }

        [DataMember]
        public string InvoiceFirstName { get; set; }

        [DataMember]
        public string InvoiceLastName { get; set; }

        [DataMember]
        public string InvoicePhone { get; set; }

        [DataMember]
        public string InvoiceCompanyName { get; set; }

        [DataMember]
        public string InvoiceAddress1 { get; set; }

        [DataMember]
        public string InvoiceAddress2 { get; set; }

        [DataMember]
        public string InvoiceTown { get; set; }

        [DataMember]
        public string InvoiceCounty { get; set; }

        [DataMember]
        public string InvoicePostcode { get; set; }

        [DataMember]
        public string InvoiceCountry { get; set; }

        [DataMember]
        public decimal? InvoiceVatRate { get; set; }
    }
}
