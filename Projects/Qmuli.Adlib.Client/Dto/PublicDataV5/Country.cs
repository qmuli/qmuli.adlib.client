﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class Country
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool Enabled { get; set; }

        [DataMember]
        public int Order { get; set; }
    }
}
