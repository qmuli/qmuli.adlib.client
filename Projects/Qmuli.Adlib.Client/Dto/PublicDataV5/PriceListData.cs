﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class PriceListData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public decimal AllInPrice { get; set; }

        [DataMember]
        public decimal FixPrice { get; set; }

        [DataMember]
        public decimal DeliveryPrice { get; set; }

        [DataMember]
        public decimal ColorCorrectionPrice { get; set; }

        [DataMember]
        public decimal PdfConversionPrice { get; set; }

        [DataMember]
        public decimal InDesignPrice { get; set; }

        [DataMember]
        public string AdExpressInfo { get; set; }

        [DataMember]
        public decimal ImageConversionPrice { get; set; }

        [DataMember]
        public int AdfastPriceListId { get; set; }
    }
}
