﻿namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    public class TitleSection
    {
        public string Title { get; set; }
        public int SectionId { get; set; }
    }
}
