﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class PublicationChangedData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public bool IsPublished { get; set; }

        [DataMember]
        public DateTime LastUpdatedDate { get; set; }

        [DataMember]
        public bool DeliverableViaAdfast { get; set; }

        [DataMember]
        public bool VisibleInAdminPlus { get; set; }
    }
}
