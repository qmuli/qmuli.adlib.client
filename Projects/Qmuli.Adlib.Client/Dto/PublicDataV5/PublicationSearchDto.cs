﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class PublicationSearchDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int PublisherId { get; set; }

        [DataMember]
        public int PressCentreId { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public string CountryName { get; set; }

        [DataMember]
        public string CountryFlagUrl { get; set; }

        [DataMember]
        public string Medium { get; set; }

        [DataMember]
        public string PublicationType { get; set; }
    }
}
