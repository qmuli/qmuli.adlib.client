﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class PressSectionDeliveryMethodsData
    {
//        UPDATE [dbo].[PressSectionDeliveryMethods]
//   SET [PressSectionDeliveryMethodType] = <PressSectionDeliveryMethodType, int,>
//      ,[Url] = <Url, nvarchar(255),>
//      ,[Email] = <Email, nvarchar(255),>
//      ,[FtpAddress] = <FtpAddress, nvarchar(255),>
//      ,[FtpUsername] = <FtpUsername, nvarchar(128),>
//      ,[FtpPassword] = <FtpPassword, nvarchar(128),>
//      ,[FtpPath] = <FtpPath, nvarchar(255),>
//      ,[FtpPort] = <FtpPort, int,>
//      ,[Name] = <Name, nvarchar(max),>
//      ,[PressSectionId] = <PressSectionId, int,>
//      ,[PressSectionExternalDeliveryMethodId] = <PressSectionExternalDeliveryMethodId, int,>
//      ,[Color] = <Color, bit,>
//      ,[Mono] = <Mono, bit,>
//      ,[Enabled] = <Enabled, bit,>
//      ,[TimeOut] = <TimeOut, nvarchar(255),>
//      ,[LastUpdatedDate] = <LastUpdatedDate, datetimeoffset(7),>
//      ,[LastUpdatedByContactId] = <LastUpdatedByContactId, int,>
//      ,[DigitalSizeId] = <DigitalSizeId, int,>
// WHERE <Search Conditions,,>
//GO

        public PressSectionDeliveryMethodsData()
        {
            Id = 0;
            Url = String.Empty;
            PressSectionDeliveryMethodType = String.Empty;
            Email = String.Empty;
            FtpAddress = String.Empty;
            FtpUsername = String.Empty;
            FtpPassword = String.Empty;
            FtpPath = String.Empty;
            FtpPort = 0;
            Name = String.Empty;
            PressSectionExternalDeliveryMethod = String.Empty;
            Color = false;
            Mono = false;
            Enabled = false;
            TimeOut = String.Empty;  
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string PressSectionDeliveryMethodType { get; set; }

        [DataMember]
        public string Email { get; set; }

         [DataMember]
        public string FtpAddress { get; set; }

         [DataMember]
         public string FtpUsername { get; set; }

         [DataMember]
         public string FtpPassword { get; set; }

         [DataMember]
         public string FtpPath { get; set; }

         [DataMember]
         public int FtpPort { get; set; }

         [DataMember]
         public string Name { get; set; }

         [DataMember]
         public string PressSectionExternalDeliveryMethod { get; set; }

        [DataMember]
        public bool Color { get; set; }

        [DataMember]
        public bool Mono { get; set; }

        [DataMember]
        public bool Enabled { get; set; } 

        [DataMember]
        public string TimeOut { get; set; }
       
    }
}
