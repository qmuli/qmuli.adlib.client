﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class PaperMuleMappingInfo
    {
        public PaperMuleMappingInfo()
        {
            PressCentreId = 0;
            PressCentreName = string.Empty;
            PublicationId = 0;
            PublicationName = string.Empty;
            PublicationSectionId = 0;
            PublicationSectionName = string.Empty;
            PublicationSectionSizeId = 0;
            PublicationSectionSizeName = string.Empty;
        }

        [DataMember]
        public int PressCentreId { get; set; }

        [DataMember]
        public string PressCentreName { get; set; }

        [DataMember]
        public int PublicationId { get; set; }

        [DataMember]
        public string PublicationName { get; set; }

        [DataMember]
        public int PublicationSectionId { get; set; }

        [DataMember]
        public string PublicationSectionName { get; set; }

        [DataMember]
        public int PublicationSectionSizeId { get; set; }

        [DataMember]
        public string PublicationSectionSizeName { get; set; }
    }
}
