﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV5
{
    [Serializable, DataContract]
    public class PublicationPressSection
    {
        public PublicationPressSection()
        {
            Id = 0;
            Name = String.Empty;
            Sizes = new List<PublicationSectionSize>();
            MaximumColumnHeight = 0;
            NumberOfColumns = 0;
            PdfStandard = String.Empty;
            DeliveryMethod = String.Empty;
            AdfastWorkflowId = 0;
            AdfastWorkflow = String.Empty;
            NewspaperPublicationDates = new List<NewspaperPublicationDateData>();
            MagazinePublicationDates = new List<MagazinePublicationDateData>();
            PressSectionDeliveryMethods = new List<PressSectionDeliveryMethodsData>();
            ProductionData = new PressProductionData();
            CustomParameters = new Dictionary<string, string>();
            PaidByPublisher = false;
            PrePressPhone = string.Empty;
            PublicationContacts = new List<PublicationContactDto>();
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int AdlibPublicationId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<PublicationSectionSize> Sizes { get; set; }

        [DataMember]
        public float MaximumColumnHeight { get; set; }

        [DataMember]
        public int NumberOfColumns { get; set; }

        [DataMember]
        public string PdfStandard { get; set; }

        [DataMember]
        public string PressColorIccProfile { get; set; }

        [DataMember]
        public string PressColorTacMaximumInkPercentage { get; set; }

        [DataMember]
        public string PressDoublePageSpreadOption { get; set; }

        [DataMember]
        public string PressMonoIccProfile { get; set; }

        [DataMember]
        public string PublicationCode { get; set; }

        [DataMember]
        public string AdfastPublicationName { get; set; }

        [DataMember]
        public int AdfastPublicationId { get; set; }

        [DataMember]
        public string DeliveryMethod { get; set; }

        [DataMember]
        public int AdfastWorkflowId { get; set; }

        [DataMember]
        public string AdfastWorkflow { get; set; }

        [DataMember]
        public string PublisherNotes { get; set; }

        [DataMember]
        public string PublicationFrequency { get; set; }

        [DataMember]
        public List<NewspaperPublicationDateData> NewspaperPublicationDates { get; set; }

        [DataMember]
        public List<MagazinePublicationDateData> MagazinePublicationDates { get; set; }

        [DataMember]
        public List<PressSectionDeliveryMethodsData> PressSectionDeliveryMethods { get; set; }

        [DataMember]
        public PressProductionData ProductionData { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public bool DeliverableViaAdfast { get; set; }

        [DataMember]
        public bool VisibleInAdminPlus { get; set; }

        [DataMember]
        public bool IsPublished { get; set; }

        [DataMember]
        public string AlertEmail { get; set; }

        [DataMember]
        public string DeliverToExternalEmail { get; set; }

        [DataMember]
        public int MediumId { get; set; }

        [DataMember]
        public string MediumName { get; set; }

        [DataMember]
        public int TypeId { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        #region contact details
        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string AddressLine3 { get; set; }

        [DataMember]
        public string AddressTown { get; set; }

        [DataMember]
        public string AddressCounty { get; set; }

        [DataMember]
        public string AddressPostcode { get; set; }

        [DataMember]
        public string AddressCountry { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string WebAddress { get; set; }
        #endregion contact details

        [DataMember]
        public string PressColorSpecification { get; set; }

        [DataMember]
        public string Deadline { get; set; }

        /// <summary>
        /// Minimum Image Resolution Peoperty Name
        /// (See MinimumImageResolutionValue for int value)
        /// </summary>
        [DataMember]
        public string MinimumImageResolution { get; set; }

        /// <summary>
        /// Minimum Bitmap Resolution Property Name
        /// (See MinimumBitmapResolutionValue for value)
        /// </summary>
        [DataMember]
        public string MinimumBitmapResolution { get; set; }

        [DataMember]
        public string GutterSizeType { get; set; }

        [DataMember]
        public decimal GutterSize { get; set; }

        [DataMember]
        public string PublicationProofRequired { get; set; }

        [DataMember]
        public bool ColumnSizesAtTop { get; set; }

        [DataMember]
        public string DefaultPressSizeType { get; set; }

        [DataMember]
        public bool ShowColumns2PageSpread { get; set; }

        [DataMember]
        public IDictionary<string, string> CustomParameters { get; set; }

        /// <summary>
        /// Indicates the processing and delivery cost is paid for by the publisher
        /// </summary>
        [DataMember]
        public bool PaidByPublisher { get; set; }

        /// <summary>
        /// The Pre Press Phone number to call when a URN is Mandatory
        /// </summary>
        [DataMember]
        public string PrePressPhone { get; set; }

        /// <summary>
        /// Minimum Image Resolution Value
        /// </summary>
        [DataMember]
        public int MinimumImageResolutionValue { get; set; }

        /// <summary>
        /// Minimum Bitmap Resolution Value
        /// </summary>
        [DataMember]
        public int MinimumBitmapResolutionValue { get; set; }

        /// <summary>
        /// Publication Contacts
        /// </summary>
        [DataMember]
        public List<PublicationContactDto> PublicationContacts { get; set; }

        [DataMember]
        public bool? PadnRequired { get; set; }

        [DataMember]
        public bool? UrnMandatory { get; set; }

        [DataMember]
        public bool? PadnCutOffTimeEnabled { get; set; }

        /// <summary>
        /// Padn Cut-off time in total seconds
        /// </summary>
        [DataMember]
        public int? PadnCutOffTime { get; set; }

        [DataMember]
        public int? BillingUnitId { get; set; }
    }
}