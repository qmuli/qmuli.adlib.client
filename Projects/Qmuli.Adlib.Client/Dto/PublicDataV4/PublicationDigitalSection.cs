﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class PublicationDigitalSection
    {
        public PublicationDigitalSection()
        {
            Id = 0;
            Name = String.Empty;
            TypeId = 0;
            TypeName = String.Empty;
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int TypeId { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<DigitalSize> Sizes  { get; set; }
    }
}