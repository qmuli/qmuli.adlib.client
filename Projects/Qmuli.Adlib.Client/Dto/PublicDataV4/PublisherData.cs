﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class PublisherData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string AddressLine3 { get; set; }

        [DataMember]
        public string AddressTown { get; set; }

        [DataMember]
        public string AddressCounty { get; set; }

        [DataMember]
        public string AddressPostcode { get; set; }

        [DataMember]
        public string AddressCountry { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string WebAddress { get; set; }

        [DataMember]
        public string LogoImageUrl { get; set; }

        [DataMember]
        public int MediaOwnerId { get; set; }

        [DataMember]
        public string MediaOwnerName { get; set; }

        [DataMember]
        public bool PublicationsDeliverableViaAdfast { get; set; }
    }
}
