﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class PublicationData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string AddressLine3 { get; set; }

        [DataMember]
        public string AddressTown { get; set; }

        [DataMember]
        public string AddressCounty { get; set; }

        [DataMember]
        public string AddressPostcode { get; set; }

        [DataMember]
        public string AddressCountry { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string WebAddress { get; set; }

        [DataMember]
        public string ProductionSpecsWebAddress { get; set; }

        [DataMember]
        public string LogoImageUrl { get; set; }

        [DataMember]
        public string CoverImageUrl { get; set; }

        [DataMember]
        public string PublicationGroupName { get; set; }

        [DataMember]
        public DateTime LastUpdatedDate { get; set; }

        [DataMember]
        public int MediaTypeId { get; set; }

        [DataMember]
        public string MediaTypeName { get; set; }

        [DataMember]
        public int PublicationTypeId { get; set; }

        [DataMember]
        public string PublicationTypeName { get; set; }

        [DataMember]
        public int PublisherId { get; set; }

        [DataMember]
        public string PublisherName { get; set; }

        [DataMember]
        public int AdfastGroupId { get; set; }

        [DataMember]
        public string PubCode { get; set; }

        [DataMember]
        public bool CorrectColor { get; set; }

        [DataMember]
        public bool CorrectPdf { get; set; }

        [DataMember]
        public bool MandatoryConvert { get; set; }

        [DataMember]
        public bool UrnMandatory { get; set; }

        [DataMember]
        public string ContactName { get; set; }
    }
}