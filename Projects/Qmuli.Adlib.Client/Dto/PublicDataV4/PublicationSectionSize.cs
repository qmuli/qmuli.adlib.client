﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class PublicationSectionSize
    {
        public PublicationSectionSize()
        {
            Id = 0;
            Name = String.Empty;
            Type = 0;
            TypeName = String.Empty;
            Width = 0;
            Height = 0;
            TypeAreaWidth = 0;
            TypeAreaHeight = 0;
            TrimWidth = 0;
            TrimHeight = 0;
            BleedWidth = 0;
            BleedHeight = 0;
            PdfWidth = 0;
            PdfHeight = 0;
            BleedTop = 0;
            BleedBottom = 0;
            BleedLeft = 0;
            BleedRight = 0;
            IsColumn = false;
            CropMarks = false;
            Columns = 0;
            SpaceOutsideTrim = 0;
            BleedDetails = String.Empty;
            InDesignOffset = 0;
            AdfastPublicationSizeId = 0;
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public float Width { get; set; }

        [DataMember]
        public float Height { get; set; }

        [DataMember]
        public float TypeAreaWidth { get; set; }

        [DataMember]
        public float TypeAreaHeight { get; set; }
        
        [DataMember]
        public float TrimWidth { get; set; }

        [DataMember]
        public float TrimHeight { get; set; }

        [DataMember]
        public float BleedWidth { get; set; }

        [DataMember]
        public float BleedHeight { get; set; }

        [DataMember]
        public float PdfWidth { get; set; }

        [DataMember]
        public float PdfHeight { get; set; }

        [DataMember]
        public float BleedTop { get; set; }

        [DataMember]
        public float BleedBottom { get; set; }

        [DataMember]
        public float BleedLeft { get; set; }

        [DataMember]
        public float BleedRight { get; set; }

        [DataMember]
        public bool IsColumn { get; set; }

        [DataMember]
        public bool CropMarks { get; set; }

        [DataMember]
        public int Columns { get; set; }

        [DataMember]
        public float SpaceOutsideTrim { get; set; }

        [DataMember]
        public string BleedDetails { get; set; }

        [DataMember]
        public float InDesignOffset { get; set; }

        [DataMember]
        public int AdfastPublicationSizeId { get; set; }
    }
}
