﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class MagazinePublicationDateData
    {
        public MagazinePublicationDateData()
        {
            Issue = String.Empty;
            CopyDeadlineDate = String.Empty;
            OnSaleDate = String.Empty;
            CoverDate = String.Empty;
        }

        [DataMember]
        public string Issue { get; set; }

        [DataMember]
        public string CopyDeadlineDate { get; set; }

        [DataMember]
        public string OnSaleDate { get; set; }

        [DataMember]
        public string CoverDate { get; set; }
    }
}
