﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class PressProductionData
    {
        public PressProductionData()
        {
            ColorWorkflowData = null;
            MonoWorkflowData = null;
            PublicationProofCurve = String.Empty;
            PressColorTacMaximumInkPercentage = 0;
            FtpConfirmDelivery = false;
            FtpPath = String.Empty;
            FtpAddress = String.Empty;
            FtpPassword = String.Empty;
            FtpPort = String.Empty;
            FtpTimeout = String.Empty;
            FtpUsername = String.Empty;
            AdfastPublicationRule = String.Empty;
            PriceList = null;
            AdfastDeliveryMethod = String.Empty;
        }

        [DataMember]
        public ProductionWorkflowData ColorWorkflowData { get; set; }

        [DataMember]
        public ProductionWorkflowData MonoWorkflowData { get; set; }

        [DataMember]
        public string PublicationProofCurve { get; set; }

        [DataMember]
        public int PressColorTacMaximumInkPercentage { get; set; }

        [DataMember]
        public bool FtpConfirmDelivery { get; set; }

        [DataMember]
        public string FtpPath { get; set; }

        [DataMember]
        public string FtpAddress { get; set; }

        [DataMember]
        public string FtpPassword { get; set; }

        [DataMember]
        public string FtpPort { get; set; }

        [DataMember]
        public string FtpTimeout { get; set; }

        [DataMember]
        public string FtpUsername { get; set; }

        [DataMember]
        public string AdfastPublicationRule { get; set; }

        [DataMember]
        public PriceListData PriceList { get; set; }

        [DataMember]
        public string AdfastDeliveryMethod { get; set; }
    }
}
