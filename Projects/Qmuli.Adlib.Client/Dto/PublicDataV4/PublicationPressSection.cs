﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class PublicationPressSection
    {
        public PublicationPressSection()
        {
            Id = 0;
            Name = String.Empty;
            Sizes = new List<PublicationSectionSize>();
            MaximumColumnHeight = 0;
            NumberOfColumns = 0;
            PdfStandard = String.Empty;
            DeliveryMethod = String.Empty;
            AdfastWorkflowId = 0;
            AdfastWorkflow = String.Empty;
            NewspaperPublicationDates = new List<NewspaperPublicationDateData>();
            MagazinePublicationDates = new List<MagazinePublicationDateData>();
            ProductionData = new PressProductionData();
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<PublicationSectionSize> Sizes { get; set; }

        [DataMember]
        public float MaximumColumnHeight { get; set; }

        [DataMember]
        public int NumberOfColumns { get; set; }

        [DataMember]
        public string PdfStandard { get; set; }

        [DataMember]
        public string AdfastPublicationName { get; set; }

        [DataMember]
        public int AdfastPublicationId { get; set; }

        [DataMember]
        public string DeliveryMethod { get; set; }

        [DataMember]
        public int AdfastWorkflowId { get; set; }

        [DataMember]
        public string AdfastWorkflow { get; set; }

        [DataMember]
        public string PublisherNotes { get; set; }

        [DataMember]
        public string PublicationFrequency { get; set; }

        [DataMember]
        public List<NewspaperPublicationDateData> NewspaperPublicationDates { get; set; }

        [DataMember]
        public List<MagazinePublicationDateData> MagazinePublicationDates { get; set; }

        [DataMember]
        public PressProductionData ProductionData { get; set; }
    }
}