﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class NewspaperPublicationDateData
    {
        public NewspaperPublicationDateData()
        {
            Day = 0;
            DaysPriorToCopyDeadline = 0;
            Time = String.Empty;
        }

        [DataMember]
        public int Day { get; set; }

        [DataMember]
        public int DaysPriorToCopyDeadline { get; set; }

        [DataMember]
        public string Time { get; set; }
    }
}
