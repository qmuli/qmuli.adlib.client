﻿using System;
using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    [Serializable, DataContract]
    public class DigitalSize
    {
        public DigitalSize()
        {
            Id = 0;
            Name = String.Empty;
            Width = 0;
            Height = 0;
            ExpandableWidth = 0;
            ExpandableHeight = 0;
            FlashSupported = false;
            VideoSupported = false;
            MaximumFileSizeText = String.Empty;
            MaximumFileSize = 0;
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public float Width { get; set; }

        [DataMember]
        public float Height { get; set; }

        [DataMember]
        public float ExpandableWidth { get; set; }

        [DataMember]
        public float ExpandableHeight { get; set; }

        [DataMember]
        public bool FlashSupported { get; set; }

        [DataMember]
        public bool VideoSupported { get; set; }

        [DataMember]
        public string ExtraSizeInformation { get; set; }

        [DataMember]
        public int MaximumFileSize { get; set; }

        [DataMember]
        public string MaximumFileSizeText { get; set; }
    }
}
