﻿using System.Runtime.Serialization;

namespace Qmuli.Adlib.Client.Dto.PublicDataV4
{
    public class ProductionWorkflowData
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember] 
        public string IccProfile { get; set; }

        [DataMember]
        public string Profile1 { get; set; }

        [DataMember]
        public string Profile2 { get; set; }

        [DataMember]
        public string Profile3 { get; set; }

        [DataMember]
        public string CheckUpload { get; set; }

        [DataMember]
        public string CheckFix { get; set; }

        [DataMember]
        public string CheckColor { get; set; }

        [DataMember]
        public string Fix1Profile { get; set; }

        [DataMember]
        public string Fix2Profile { get; set; }

        [DataMember]
        public string Fix3Profile { get; set; }

    }
}