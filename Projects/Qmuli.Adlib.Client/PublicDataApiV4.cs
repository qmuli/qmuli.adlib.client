﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Qmuli.Adlib.Client.Dto;
using Qmuli.Adlib.Client.Dto.PublicDataV4;

namespace Qmuli.Adlib.Client
{
    /// <summary>
    /// Adlib Public Data Api Client (For Version 4)
    /// </summary>
    public static class PublicDataApiV4
    {
        /// <summary>
        /// Gets the session Key for the specified credentials
        /// </summary>
        /// <returns></returns>
        public static string GetSessionKey(string password)
        {
            using (var client = GetHttpClient())
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("ApiKey", Settings.QmuliAdlibPublicDataApiKey),
                    new KeyValuePair<string, string>("Username", Settings.QmuliAdlibPublicDataApiUsername),
                    new KeyValuePair<string, string>("Password", password)
                });

                var response = client.PostAsync("GetSessionKey/", content).Result;

                var result = String.Empty;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<string>().Result ?? String.Empty;
                }

                return result;
            }
        }

        /// <summary>
        /// Checks is the supplied headers are valid
        /// </summary>
        /// <returns></returns>
        public static bool IsSessionKeyValid()
        {
            using (var client = GetHttpClient())
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("", "")
                });

                var response = client.PostAsync("IsSessionKeyValid/", content).Result;

                var result = false;

                if (response.IsSuccessStatusCode)
                {
                    result = (response.Content.ReadAsAsync<bool>().Result.ToString().ToLower() == "true");
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the date for the most recent publication update
        /// </summary>
        /// <returns></returns>
        public static DateTimeOffset GetPublicationMostRecentUpdatedDate()
        {
            using (var client = GetHttpClient())
            {   
                var response = client.GetAsync("GetPublicationMostRecentUpdatedDate/").Result;

                var result = DateTimeOffset.MinValue;

                if (response.IsSuccessStatusCode)
                {
                    result = DateTimeOffset.Parse(response.Content.ReadAsAsync<string>().Result);
                }

                return result;
            }
        }

        /// <summary>
        ///  	Gets the Publications Details, including Id, name, contact details and image urls for the related publication Id
        /// </summary>
        /// <param name="publicationId">PublicationId</param>
        /// <returns></returns>
        public static PublisherData GetPublisherDataFromPublicationId(int publicationId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetPublisherFromPublicationId/?PublicationId={0}", publicationId)).Result;

                var result = new PublisherData();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<PublisherData>().Result;
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the Publisher Details, including Id, name, contact details and image urls for the specified publisher Id
        /// </summary>
        /// <param name="publisherId">PublisherId</param>
        /// <returns></returns>
        public static PublisherData GetPublisher(int publisherId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetPublisher/?PublisherId={0}", publisherId)).Result;

                var result = new PublisherData();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<PublisherData>().Result;
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the Publisher Details List from the Adlib Public Data Api
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static List<PublisherData> GetPublisherList(string query)
        {
            using (var client = GetHttpClient())
            {   
                var response = client.GetAsync(String.Concat("GetPublisherList/?Query=", query)).Result;

                var result = new List<PublisherData>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<PublisherData>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the Publisher Select List from the Adlib Public Data Api
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="publisherId">Publisher Id</param>
        /// <param name="onlyReturnPublicationsDeliverableByAdfast"></param>
        /// <returns>SelectListItem List</returns>
        public static List<SelectListOption> GetPublisherSelectList(string query, int? publisherId, bool? onlyReturnPublicationsDeliverableByAdfast)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetPublisherSelectList/?Query={0}&PublisherId={1}&OnlyReturnPublicationsDeliverableByAdfast={2}", query,  publisherId, onlyReturnPublicationsDeliverableByAdfast)).Result;

                var result = new List<SelectListOption>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<SelectListOption>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the Publication Details List list from the Adlib Public Data Api
        /// </summary>
        /// <param name="publisherId"></param>
        /// <param name="query"></param>
        /// <param name="fromLastUpdatedDate"></param>
        /// <returns></returns>
        public static List<PublicationData> GetPublicationList(int? publisherId, string query, DateTime? fromLastUpdatedDate)
        {
            using (var client = GetHttpClient())
            {
                var response =
                    client.GetAsync(String.Format("GetPublicationList/?PublisherId={0}&Query={1}&FromLastUpdatedDate={2}", publisherId,
                    query, (fromLastUpdatedDate.HasValue ?  fromLastUpdatedDate.Value.ToString("G") : String.Empty))).Result;

                var result = new List<PublicationData>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<PublicationData>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        ///  	Gets the Publications Details, including Id, name, contact details and image urls for the specified publication Id
        /// </summary>
        /// <param name="publicationId">PublicationId</param>
        /// <returns></returns>
        public static PublicationData GetPublication(int publicationId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetPublication/?PublicationId={0}", publicationId)).Result;

                var result = new PublicationData();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<PublicationData>().Result;
                }

                return result;
            }
        }

        /// <summary>
        /// Returns a list of updated Publications Details from the specified date
        /// (updated date could include date of publication or related data, eg section and size data)
        /// </summary>
        /// <param name="fromDate">The minimum date to check for updated data in the format: dd/MM/yyyy hh:mm:ss</param>
        /// <returns></returns>
        public static List<PublicationChangedData> GetUpdatedPublicationList(string fromDate)
        {
            using (var client = GetHttpClient())
            {
                var response =
                    client.GetAsync(String.Format("GetUpdatedPublicationList/?FromDate={0}", fromDate)).Result;

                var result = new List<PublicationChangedData>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<PublicationChangedData>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the Publication Id List list from the last updated date
        /// </summary>
        /// <param name="publisherId"></param>
        /// <param name="query"></param>
        /// <param name="fromLastUpdatedDate"></param>
        /// <returns></returns>
        public static List<int> GetPublicationIdListFromLastUpdatedDate(int? publisherId, string query, DateTime? fromLastUpdatedDate)
        {
            using (var client = GetHttpClient())
            {
                var response =
                    client.GetAsync(String.Format("GetPublicationIdListFromLastUpdatedDate/?FromDate={0}", 
                    (fromLastUpdatedDate.HasValue ?  fromLastUpdatedDate.Value.ToString("G") : String.Empty))).Result;

                var result = new List<int>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<int>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the Publication Id List list from the last updated date, deliverable by Adfast
        /// </summary>
        /// <param name="publisherId"></param>
        /// <param name="query"></param>
        /// <param name="fromLastUpdatedDate"></param>
        /// <returns></returns>
        public static List<int> GetPublicationIdListDeliverableByAdfastFromLastUpdatedDate(int? publisherId, string query, DateTime? fromLastUpdatedDate)
        {
            using (var client = GetHttpClient())
            {
                var response =
                    client.GetAsync(String.Format("GetPublicationIdListDeliverableByAdfastFromLastUpdatedDate/?FromDate={0}", 
                    (fromLastUpdatedDate.HasValue ?  fromLastUpdatedDate.Value.ToString("G") : String.Empty))).Result;

                var result = new List<int>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<int>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the Publication Select List list from the Adlib Public Data Api
        /// </summary>
        /// <param name="publisherId"></param>
        /// <param name="query"></param>
        /// <param name="onlyReturnPublicationsDeliverableViaAdfast"></param>
        /// <returns></returns>
        public static List<SelectListOption> GetPublicationSelectList(int? publisherId, string query, bool? onlyReturnPublicationsDeliverableViaAdfast)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetPublicationSelectList/?PublisherId={0}&Query={1}&OnlyReturnPublicationsDeliverableViaAdfast=", publisherId, query, onlyReturnPublicationsDeliverableViaAdfast)).Result;

                var result = new List<SelectListOption>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<SelectListOption>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the publication press sections and related sizes
        /// </summary>
        /// <param name="publicationId">Publication Id</param>
        /// <param name="units">Units of measure (Inches or Millimeters)</param>
        /// <returns></returns>
        public static List<PublicationPressSection> GetPublicationPressSections(int publicationId, string units)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetPublicationPressSections/?PublicationId={0}&Units={1}", publicationId, units)).Result;

                var result = new List<PublicationPressSection>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<PublicationPressSection>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the publication digital sections and related sizes
        /// </summary>
        /// <param name="publicationId">Publication Id</param>
        /// <returns></returns>
        public static List<PublicationDigitalSection> GetPublicationDigitalSections(int publicationId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetPublicationDigitalSections/?PublicationId={0}", publicationId)).Result;

                var result = new List<PublicationDigitalSection>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<PublicationDigitalSection>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the publication digital sections sizes
        /// </summary>
        /// <param name="publicationSectionId">Publication Section Id (digital section)</param>
        /// <returns></returns>
        public static List<DigitalSize> GetPublicationDigitalSectionSizes(int publicationSectionId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetPublicationDigitalSectionSizes/?DigitalSectionId={0}", publicationSectionId)).Result;

                var result = new List<DigitalSize>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<DigitalSize>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Returns a list of digital publication sections for the purpose of populating a select list
        /// </summary>
        /// <param name="publicationId">Search query</param>
        /// <returns>SelectListItem List</returns>
        public static List<SelectListOption> GetPublicationDigitalSectionSelectList(int publicationId)
        {
           using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Concat("GetPublicationDigitalSectionSelectList/?PublicationId=", publicationId)).Result;

                var result = new List<SelectListOption>();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<IEnumerable<SelectListOption>>().Result.ToList();
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the Digital Size for the specified digitalSizeId
        /// </summary>
        /// <param name="digitalSizeId">Search query</param>
        public static DigitalSize GetPublicationSectionDigitalSize(int digitalSizeId)
        {
           using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Concat("GetPublicationSectionDigitalSize/?DigitalSectionSizeId=", digitalSizeId)).Result;

                var result = new DigitalSize();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<DigitalSize>().Result;
                }

                return result;
            }
        }

        /// <summary>
        /// Updates the Publication Press Section AdfastId mapping to Adfast (This is used by Qmuli internally, this call is not available to standard users)
        /// </summary>
        /// <param name="adlibPressSectionId">Adfast Press Section Id</param>
        /// <param name="adfastPublicationId">Adfast Publication Id</param>
        /// <returns></returns>
        public static void UpdatePublicationPressSectionAdfastIdMappingReferenceInAdlib(int adlibPressSectionId, int adfastPublicationId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.PostAsync(String.Format("UpdatePublicationPressSectionAdfastId/?PressSectionId={0}&AdfastPublicationId={1}", adlibPressSectionId, adfastPublicationId), null).Result;

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception("Error updating Adlib Press Section AdfastId mapping");
                }

            }
        }

        /// <summary>
        /// Updates the Publication Press Section Size AdfastId mapping to Adfast (This is used by Qmuli internally, this call is not available to standard users)
        /// </summary>
        /// <param name="adlibPressSectionSizeId">Adfast Press Section Size Id</param>
        /// <param name="adfastPublicationSizeId">Adfast Publication Size Id</param>
        /// <returns></returns>
        public static void UpdatePublicationPressSectionSizeAdfastIdMappingReferenceInAdlib(int adlibPressSectionSizeId, int adfastPublicationSizeId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.PostAsync(String.Format("UpdatePublicationPressSectionSizeAdfastId/?PressSectionSizeId={0}&AdfastPublicationSizeId={1}", adlibPressSectionSizeId, adfastPublicationSizeId), null).Result;

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception("Error updating Adlib Press Section AdfastId mapping");
                }
            }
        }

        /// <summary>
        /// Updates the Publisher GroupId mapping to Adfast (This is used by Qmuli internally, this call is not available to standard users)
        /// </summary>
        /// <param name="adlibPublisherId"></param>
        /// <param name="adfastGroupId"></param>
        public static void UpdatePublisherAdfastGroupIdMappingReferenceInAdlib(int adlibPublisherId, int adfastGroupId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.PostAsync(String.Format("UpdatePublisherAdfastGroupId/?PublisherId={0}&AdfastGroupId={1}", adlibPublisherId, adfastGroupId), null).Result;

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception("Error updating Adlib Publisher Adfast GroupId mapping");
                }
            }
        }

        /// <summary>
        /// Gets the Digital Section pricing data for delivery and production (This is used by Qmuli internally, this call is not available to standard users)
        /// </summary>
        /// <param name="digitalSectionId">Digital Section Id</param>
        /// <returns></returns>
        public static PriceListData GetDigitalSectionPricing(int digitalSectionId)
        {
            using (var client = GetHttpClient())
            {
                var response = client.GetAsync(String.Format("GetDigitalSectionPricing/?DigitalSectionId={0}", digitalSectionId)).Result;

                var result = new PriceListData();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<PriceListData>().Result;
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the full api version number in format x.x.x
        /// </summary>
        /// <returns></returns>
        public static string GetApiVersion()
        {
            using (var client = GetHttpClient())
            {   
                var response = client.GetAsync("GetApiVersion/").Result;

                var result = String.Empty;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<string>().Result ?? String.Empty;
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the HttpClient and sets the required headers, with config values
        /// </summary>
        /// <returns></returns>
        private static HttpClient GetHttpClient()
        {
            var client = new HttpClient {BaseAddress = new Uri(Settings.QmuliAdlibPublicDataApiUrl)};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("SessionKey", Settings.QmuliAdlibPublicDataSessionKey);
            client.DefaultRequestHeaders.Add("Username", Settings.QmuliAdlibPublicDataApiUsername);
            return client;
        }
    }
}
