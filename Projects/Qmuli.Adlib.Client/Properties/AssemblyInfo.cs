﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Qmuli.Adlib.Client")]
[assembly: AssemblyDescription("Qmuli Adlib Client Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Qmuli Limited")]
[assembly: AssemblyProduct("Qmuli.Adlib.Client")]
[assembly: AssemblyCopyright("Copyright © 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("beaf50b7-2551-4076-b243-eee8ff94b3a5")]
[assembly: AssemblyVersion("5.2.8.0")]
[assembly: AssemblyFileVersion("5.2.8.0")]
