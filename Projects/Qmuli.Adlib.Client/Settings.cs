﻿using System;
using System.Configuration;

namespace Qmuli.Adlib.Client
{
    public static class Settings
    {
        private static string _url;
        private static string _apiKey;
        private static string _username;
        private static string _sessionKey;

        public static string QmuliAdlibPublicDataApiUrl
        {
            get
            {
                if (_url == null)
                {
                    var result = ConfigurationManager.AppSettings["Qmuli.Adlib.PublicDataApiUrl"];
                    return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
                }

                return _url;
            }
            set { _url = value; }
        }

        public static string QmuliAdlibPublicDataApiKey
        {
            get
            {
                if (_apiKey == null)
                {
                    var result = ConfigurationManager.AppSettings["Qmuli.Adlib.PublicDataApiKey"];
                    return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
                }

                return _apiKey;
            }
            set { _apiKey = value; }
        }

        public static string QmuliAdlibPublicDataApiUsername
        {
            get
            {
                if (_username == null)
                {
                    var result = ConfigurationManager.AppSettings["Qmuli.Adlib.PublicDataApiUsername"];
                    return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
                }

                return _username;
            }
            set { _username = value; }
        }

        public static string QmuliAdlibPublicDataSessionKey
        {
            get
            {
                if (_sessionKey == null)
                {
                    var result = ConfigurationManager.AppSettings["Qmuli.Adlib.PublicDataSessionKey"];
                    return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
                }

                return _sessionKey;
            }
            set { _sessionKey = value; }
        }
    }
}
