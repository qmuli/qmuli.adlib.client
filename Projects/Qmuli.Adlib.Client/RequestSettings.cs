﻿namespace Qmuli.Adlib.Client
{
    public class RequestSettings
    {
        public string QmuliAdlibPublicDataApiUrl { get; set; }
        public string QmuliAdlibPublicDataApiKey { get; set; }
        public string QmuliAdlibPublicDataApiUsername { get; set; }
        public string QmuliAdlibPublicDataSessionKey { get; set; }
    }
}
