﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class AuthTests
    {
        /// <summary>
        /// Set App.config Adib API app settings
        /// </summary>
        [TestMethod]
        public void TestAuthApiCalls()
        {
            var requestSettings = Qmuli.Adlib.Client.PublicDataApiV5.DefaultRequestSettings;

            var sessionKey = Qmuli.Adlib.Client.PublicDataApiV5.GetSessionKey(ConfigurationManager.AppSettings["Qmuli.Adlib.Password"], requestSettings);

            Assert.IsFalse(string.IsNullOrWhiteSpace(sessionKey), "Session Key is Empty");  

            // Test is Session Key is Valid
            requestSettings.QmuliAdlibPublicDataSessionKey = sessionKey;

            var sessionKeyIsValid = Qmuli.Adlib.Client.PublicDataApiV5.IsSessionKeyValid(requestSettings);

            Assert.IsTrue(sessionKeyIsValid, "Session Key is not valid");  
        }
    }
}
